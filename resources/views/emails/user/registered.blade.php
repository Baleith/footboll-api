@component('mail::message')

<h1> Hey {{$user->first_name}} '{{$user->nickname}}' {{$user->last_name}}!</h1>
<h3> Thank you for registering. </h3>

@component('mail::button', ['url' => URL::to('/')])
Visit us
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
