###Getting started
1. Clone repo
2. Run composer install
3. Set .env variables
3. Run php artisan migrate
4. Run laravel passport:install
5. For testing im using https://mailtrap.io/, set Mailtrap info in .env file.
6. Run php artisan db:seed

###Getting started with API calls
Im using Postman for API testing

Make a post request to /api/register

with following form_data

+ first_name
+ last_name
+ nickname
+ email
+ zip
+ arm
+ team
+ password
+ c_password

Should return a token, Save this token for later use.

You can retrive this token again by making a post request to api/login
form_data

+ email
+ password

All the following routes requires authentication, luckily you now have an authentication token.
Set Headers to

+ Accept: application/json
+ Authorization: Bearer 'your_token'
  
### Match_types = group, eight, quarter, semi, final
### group_letters = a, b, c ,d ,e, f, g, h
####PLAYERS
  Method  | Url 		      | Parameters 	                                                       | Function
  ------- | ----------------- | --------------------------------------------------------------------------------------------------------------- | -------------
  get 	  | api/players       | optional team(string), goals(int), red_cards(int, yellow_cards(int)                                             | Returns all players matching search queries
  put     | api/update/player | required: team(string), shirt_number(int), Optional: goals(int) yellow_cards(int), red_cards(int)               | Update a players information    						
  post    | api/store/player  | required: team(string), shirt_number(int), player(string) Optional: goals(int) yellow_cards(int), red_cards(int)| Saves a new player



####GROUP
  Method  | Url 		        | Parameters 	                                       | Function
  ------- | --------------------| -----------------------------------------------------| ---------------------------------------------------
  get 	  | api/group-standings | optional: group_letter: 'string'                     | Retrieves all teams or all teams in a group. 
  get     | api/top-group       | optional: top_index: 'int'                           | Retrieves the top x teams with the most points in a group


####TEAMS
  Method  | Url 		      | Parameters 	                                                      | Function
  ------- | ------------------| ----------------------------------------------------------------- | -------------
  get 	  | api/teams         | Optional: match_type: 'string', group_letter: 'string'            | Retrieves all/or teams in a group, with matches of match_type (match_type=all for all matches).
  post    | api/store/team    | Required: name: 'string', group_letter: 'string'                  | Stores a new team in a certain group  
  
####MATCHES
  Method  | Url 		      | Parameters 	                                                                      | Function
  ------- | ----------------- | --------------------------------------------------------------------------------- | -------------
  get 	  | api/match         | required: team_1: 'string', team_2: 'string', date: 'int:10'                      | Retrieve a match by date and team names.
  put     | api/update/match  | required: team_1: 'string', team_2: 'string', date: 'int:10'                      | Updates a match result.
	      |                   | optional: team_1_goals: 'int', team_2_goals: 'int                                 |
	      |                   |                                                                                   |    
  post    | /match-result     | required: team_1: 'string', team_2: 'string', match_type: 'string', date: 'int:10 | Store a new match. 
	      |                   | optional: team_1_goals: 'int', team_2_goals: 'int   