<?php

namespace Money\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMatch extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_1' => 'required|string|max:255',
            'team_2' => 'required|string|max:255',
            'team_1_goals' => 'nullable|numeric',
            'team_2_goals' => 'nullable|numeric',
            'date' => 'required|digits:8'
        ];
    }
}
