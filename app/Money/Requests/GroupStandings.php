<?php

namespace Money\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroupStandings extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_letter' => 'nullable|string|max:1',
            'top_index' => 'nullable|numeric',
        ];
    }
}
