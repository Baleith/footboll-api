<?php

namespace Money\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetTeams extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'match_type' => 'nullable|string|max:255',
            'group_letter' => 'nullable|string',
        ];
    }
}
