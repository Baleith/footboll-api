<?php

namespace Money\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePlayer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team' => 'required|string|max:255',
            'player' => 'required|string|max:255',
            'goals' => 'nullable|numeric',
            'yellow_cards' => 'nullable|numeric',
            'red_cards' => 'nullable|numeric',
            'shirt_number' => 'required|numeric',
        ];
    }
}
