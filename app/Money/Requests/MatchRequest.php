<?php

namespace Money\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'match_type' => 'required',
            'team_1' => 'required|string|max:255',
            'team_2' => 'required|string|max:255',
            'team_1_goals' => 'nullable|numeric',
            'team_2_goals' => 'nullable|numeric',
            'winner' => 'nullable|string|max:255',
            'draw' => 'nullable|boolean',
            'date' => 'required|digits:8'
        ];
    }
}
