<?php

namespace Money\Models;

use Illuminate\Database\Eloquent\Model;

class MatchInfo extends Model
{
    protected $fillable = [
    	'match_id', 'team_id', 'goals', 'points', 'winner', 'draw', 'date'
    ];
    
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
