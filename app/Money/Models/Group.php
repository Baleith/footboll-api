<?php

namespace Money\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model {
	protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function addTeam(Team $team)
    {	
    	$this->teams()->save($team);
    }

    public function teams() {
    	return $this->hasMany(Team::class);
    }

    public function groupResults() {
    	return $this->hasMany('Money\Models\GroupResult', 'group_id');
    }
}
