<?php

namespace Money\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $fillable = [
        'team_id', 'name', 'goals', 'shirt_number', 'red_cards', 'yellow_cards'
    ];
    protected $hidden = [
        'created_at', 'updated_at'
    ];
    
    public function team() {
    	return $this->belongsTo('Money\Models\Team', 'team_id');
    }
}
