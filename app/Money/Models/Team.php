<?php

namespace Money\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'name'
    ];
    
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public static function make($name)
    {
        return new static(compact('name'));
    }

    public function players() {
    	return $this->hasMany('Money\Models\Player','team_id');
    }

    public function points() {
    	return $this->hasMany('Money\Models\MatchInfo', 'team_id');
    }

    public function teamOne() {
        return $this->hasMany('Money\Models\Match', 'team_1_id');
    }

    public function teamTwo() {
        return $this->hasMany('Money\Models\Match', 'team_2_id');
    }
}
