<?php

namespace Money\Models;

use Illuminate\Database\Eloquent\Model;

class MatchType extends Model
{
	protected $hidden = [
        'created_at', 'updated_at'
    ];
    
    public function matches() {
    	return $this->hasMany('Money\Models\Match', 'match_type_id');
    }
}
