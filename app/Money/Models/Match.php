<?php

namespace Money\Models;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $fillable = [
		'match_type_id', 'team_1', 'team_2', 'team_1_id', 'team_2_id'
	];
	protected $hidden = [
        'created_at', 'updated_at'
    ];

	public function teams() {
		return $this->belongsTo('Money\Models\Team');
	}

	public function matchInfo() {
		return $this->hasMany('Money\Models\MatchInfo', 'match_id');
	}
}
