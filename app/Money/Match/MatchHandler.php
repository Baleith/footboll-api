<?php
namespace Money\Match;

use Exception;
use Money\Match\MatchRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MatchHandler implements MatchRepositoryInterface
{
	private $matchRepo;

    public function __construct(Match $matchRepo)
    {
    	$this->matchRepo = $matchRepo;
    }

    /**
     * Store a new match.
     * @param Request:
 	 * 	required: team_1: 'string', team_2: 'string', match_type: 'string', date: 'int'
	 *	optional: team_1_goals: 'int', team_2_goals: 'int'
     */
	public function store($matchAttributes) {
		try {
			$date = $request->date;
			$match_type = $this->getMatchType($request);
			$match = $this->getMatch($match_type, $request, $date);

			if(!$match->isEmpty()) {
				return array('Match already exists', $match);
			}

			$this->createMatch($request, $match_type); 
			
	        return 'Success';
		}
		catch(ModelNotFoundException $e) {
        	return 'Group not found';
        }
		catch(Exception $e) {
			return 'Error: '.$e;
		}
	}

	/**
     * @param Request:
 	 * 	required: match_type: 'string'
     */
	public function matchByType($match_type) {
		return $this->matchRepo->matchByType($match_type);
	}

	/**
     * @param Request:
 	 * 	required: team_1: 'string', team_2: 'string', date: 'int:10'
     */
	public function match($team_1, $team_2, $match_type) {
		return $this->matchRepo->match($team_1, $team_2, $match_type);
	}

	/**
     * @param Request:
 	 * 	required: team_1: 'string', team_2: 'string' 
 	 * @param match_type: MatchType::collection
 	 * @param teams: Team::collection
     */
	public function createTeam($match_type, $teams, $request) {
		return Match::create([
	                'match_type_id' => $match_type->id,
	                'team_1_id' => $teams[0]->id,
	                'team_2_id' => $teams[1]->id,
	                'team_1' => $request->team_1,
	                'team_2' => $request->team_2,
	            ]);
	}

	/**
     * @param Request:
 	 * 	required: team_1: 'string', team_2: 'string', date: 'int:10' 
 	 * optional: team_1_goals: 'string', team_2_goals: 'string'
 	 * @param match_type: MatchType::collection
     */
	public function createMatch($request, $match_type) {
		\DB::transaction(function() use ($request, $match_type) {
            $teams = Team::whereIn('name', [$request->team_1, $request->team_2])->get();

            $match = $this->createTeam($match_type, $teams, $request);

            $matchInfos = $this->setMatchInfo($match->id, $teams[0]->id, $teams[1]->id, $request->team_1_goals, $request->team_2_goals, $request->date);
            
            foreach ($matchInfos as $key => $matchInfo) {
                $record = MatchInfo::create($matchInfo);
            }
        });
	}

	/**
     * Updates a match result.
     * @param Request:
 	 * 	required: team_1: 'string', team_2: 'string', date: 'int'
	 *	optional: team_1_goals: 'int', team_2_goals: 'int'
     */
	public function updateMatch(UpdateMatch $request) {
		try {
			$date = $request->date;
			$match = Match::where([['team_1', $request->team_1], ['team_2', $request->team_2]])->whereHas('matchInfo', function($query) use($date){
			     $query->where('date', $date);
			})->firstOrFail();

			$m = MatchInfo::where('match_id', $match->id)->get();

			$matchInfos = $this->setMatchInfo($match->id, $match->team_1_id, $match->team_2_id, $request->team_1_goals, $request->team_2_goals, $request->date);
	          
	        foreach ($m as $key => $matchInfo) {
	            $matchInfo->update($matchInfos[$key]);
	        }
	        return array('Success', $match, $m);
        }
        catch(ModelNotFoundException $e) {
        	return 'Match not found';
        }
	}

	/**
	* Retrieve a match by date and team names.
	* @param Request:
	* required: team_1: 'string', team_2: 'string', date: 'int'
	* @return A set of match(es) 
	*/
	public function match(UpdateMatch $request) {
	    try {
	  		$date = $request->date;
	  		return Match::where([['team_1', $request->team_1], ['team_2', $request->team_2]])->whereHas('matchInfo', function($query) use($date){
	  		     $query->where('date', $date);
	  		})->with('matchInfo')->firstOrFail();
	    }
	    catch(ModelNotFoundException $e) {
      		return 'Match not found';
	    }
	}

	public function setMatchInfo($match_id, $team_1_id, $team_2_id, $goals_1 = null, $goals_2 = null, $date) {
		$matchInfos = array(
            array('match_id' => $match_id, 'team_id'=> $team_1_id, 'goals'=> $goals_1, 'points'=> null, 'winner'=> null, 'draw' => null, 'date' => $date),
            array('match_id' => $match_id, 'team_id'=> $team_2_id, 'goals'=> $goals_2, 'points'=> null, 'winner'=> null, 'draw' => null, 'date' => $date),
    	);

       	if($goals_1 > $goals_2) {
       		$matchInfos[0]['points'] = 3;
       		$matchInfos[1]['points'] = 0;
       		$matchInfos[0]['winner'] = 1;
       		$matchInfos[1]['winner'] = 0;
       		$matchInfos[0]['draw'] = 0;
       		$matchInfos[1]['draw'] = 0;
       	}
       	elseif ($goals_2 > $goals_1) {
       		$matchInfos[0]['points'] = 0;
       		$matchInfos[1]['points'] = 3;
       		$matchInfos[0]['winner'] = 0;
       		$matchInfos[1]['winner'] = 1;
       		$matchInfos[0]['draw'] = 0;
       		$matchInfos[1]['draw'] = 0;
       	}
       	elseif ($goals_2 != null || $goals_1 != null){
       		$matchInfos[0]['points'] = 1;
       		$matchInfos[1]['points'] = 1;
       		$matchInfos[0]['winner'] = 0;
       		$matchInfos[1]['winner'] = 0;
       		$matchInfos[0]['draw'] = 1;
       		$matchInfos[1]['draw'] = 1;
       	}

       	return $matchInfos;
	}
}