<?php
namespace Money\Match;

interface MatchRepositoryInterface
{
    public function matches($teamId);
    public function matchByType($match_type);
    public function match($matchAttributes);
    public function store($matchAttributes);
    public function createMatch($match_type, $teams, $matchAttributes);
    public function setMatchInfo($match_id, $team_1_id, $team_2_id, $goals_1 = null, $goals_2 = null, $date);
    public function updateMatch($matchAttributes);
    public function createMatchWithRelations($matchAttributes, $match_type);
}