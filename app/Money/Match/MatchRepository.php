<?php

namespace Money\Match;

use Money\Models\MatchType;
use Money\Models\Team;
use Money\Models\Match;
use Money\Models\MatchInfo;
/**
 * summary
 */
class MatchRepository implements MatchRepositoryInterface  
{
    private $matchModel;
    private $matchInfoModel;
    private $teamModel;
    private $matchTypeModel;

    public function __construct(Match $matchModel, MatchInfo $matchInfoModel, Team $teamModel, MatchType $matchTypeModel)
    {
        $this->matchModel = $matchModel;
        $this->matchInfoModel = $matchInfoModel;
        $this->teamModel = $teamModel;
        $this->matchTypeModel = $matchTypeModel;
    }

	public function matches($teamId) {
		return $this->matchModel::where(function ($query) use($teamId){
                $query->where('team_1_id', $teamId)
                      ->orWhere('team_2_id', $teamId);
            })
            ->with(['matchInfo' => function($query) use($teamId) {
                $query->where('team_id', $teamId)->whereIn('winner', [0, 1]);
            }])->whereHas('matchInfo', function ($query) use ($teamId) {
                $query->whereIn('winner', [0, 1])->where('team_id', $teamId);
            })->get();
	}

    /**
    * Retrieve a match by date and team names.
    * @return A set of match(es) 
    */
    public function match($matchAttributes) {
        try {
            $date = $matchAttributes['date'];
            return $this->matchModel::where([ ['team_1', $matchAttributes['team_1']], ['team_2', $matchAttributes['team_2']] ])->whereHas('matchInfo', function($query) use($date){
                 $query->where('date', $date);
            })->with('matchInfo')->firstOrFail();
        }
        catch(ModelNotFoundException $e) {
            return 'Match not found';
        }
    }

    /**
     * Store a new match.
     * @param matchAttributes:
     *  required: team_1: 'string', team_2: 'string', match_type: 'string', date: 'int'
     *  optional: team_1_goals: 'int', team_2_goals: 'int'
     */
    public function store($matchAttributes) {
        try {
            $date = $request->date;
            $match_type = $this->matchByType($matchAttributes['match_type']);
            $match = $this->match($matchAttributes);

            if(!$match->isEmpty()) {
                return array('Match already exists', $match);
            }

            $this->createMatchWithRelations($matchAttributes, $match_type); 
            
            return 'Success';
        }
        catch(ModelNotFoundException $e) {
            return 'Group not found';
        }
        catch(Exception $e) {
            return 'Error: '.$e;
        }
    }

    /**
     * @param Request:
     *  required: match_type: 'string'
     */
    public function matchByType($match_type) {
        return $this->matchTypeModel::where('type', $match_type)->firstOrFail();
    }

    /**
     * @param matchAttributes
     * @param match_type: MatchType::collection
     * @param teams: Team::collection
     */
    public function createMatch($match_type, $teams, $matchAttributes) {
        return $this->matchModel::create([
                    'match_type_id' => $match_type->id,
                    'team_1_id' => $teams[0]->id,
                    'team_2_id' => $teams[1]->id,
                    'team_1' => $matchAttributes['team_1'],
                    'team_2' => $matchAttributes['team_2'],
                ]);
    }

    /**
     * @param matchAttributes:
     *  required: team_1: 'string', team_2: 'string', date: 'int:10' 
     * optional: team_1_goals: 'string', team_2_goals: 'string'
     * @param match_type: MatchType::collection
     */
    public function createMatchWithRelations($matchAttributes, $match_type) {
        \DB::transaction(function() use ($matchAttributes, $match_type) {
            $teams = $this->teamModel::whereIn('name', [$matchAttributes['team_1'], $matchAttributes['team_2']])->get();

            $match = $this->createMatch($match_type, $teams, $matchAttributes);

            $matchInfos = $this->setMatchInfo($match->id, $teams[0]->id, $teams[1]->id, $matchAttributes['team_1'], $matchAttributes['team_2'], $matchAttributes['date']);
            
            foreach ($matchInfos as $key => $matchInfo) {
                $record = $this->matchInfoModel::create($matchInfo);
            }
        });
    }

    /**
     * Updates a match result.
     * @param matchAttributes:
     *  required: team_1: 'string', team_2: 'string', date: 'int'
     *  optional: team_1_goals: 'int', team_2_goals: 'int'
     */
    public function updateMatch($matchAttributes) {
        try {
            $date = $request->date;
            $match = $this->matchModel::where([['team_1', $request->team_1], ['team_2', $request->team_2]])->whereHas('matchInfo', function($query) use($date){
                 $query->where('date', $date);
            })->firstOrFail();

            $m = $this->matchInfoModel::where('match_id', $match->id)->get();

            $matchInfos = $this->setMatchInfo($match->id, $match->team_1_id, $match->team_2_id, $request->team_1_goals, $request->team_2_goals, $request->date);
              
            foreach ($m as $key => $matchInfo) {
                $matchInfo->update($matchInfos[$key]);
            }
            return array('Success', $match, $m);
        }
        catch(ModelNotFoundException $e) {
            return 'Match not found';
        }
    }

    public function setMatchInfo($match_id, $team_1_id, $team_2_id, $goals_1 = null, $goals_2 = null, $date) {
        $matchInfos = array(
            array('match_id' => $match_id, 'team_id'=> $team_1_id, 'goals'=> $goals_1, 'points'=> null, 'winner'=> null, 'draw' => null, 'date' => $date),
            array('match_id' => $match_id, 'team_id'=> $team_2_id, 'goals'=> $goals_2, 'points'=> null, 'winner'=> null, 'draw' => null, 'date' => $date),
        );

        if($goals_1 > $goals_2) {
            $matchInfos[0]['points'] = 3;
            $matchInfos[1]['points'] = 0;
            $matchInfos[0]['winner'] = 1;
            $matchInfos[1]['winner'] = 0;
            $matchInfos[0]['draw'] = 0;
            $matchInfos[1]['draw'] = 0;
        }
        elseif ($goals_2 > $goals_1) {
            $matchInfos[0]['points'] = 0;
            $matchInfos[1]['points'] = 3;
            $matchInfos[0]['winner'] = 0;
            $matchInfos[1]['winner'] = 1;
            $matchInfos[0]['draw'] = 0;
            $matchInfos[1]['draw'] = 0;
        }
        elseif ($goals_2 != null || $goals_1 != null){
            $matchInfos[0]['points'] = 1;
            $matchInfos[1]['points'] = 1;
            $matchInfos[0]['winner'] = 0;
            $matchInfos[1]['winner'] = 0;
            $matchInfos[0]['draw'] = 1;
            $matchInfos[1]['draw'] = 1;
        }

        return $matchInfos;
    }
}