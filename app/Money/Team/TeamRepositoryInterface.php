<?php

namespace Money\Team;

interface TeamRepositoryInterface {
	public function store($name, $group_letter);
	public function byGroup($group_letter, $q);
	public function teams($match_type, $group_letter);
	public function group($group_letter);
}