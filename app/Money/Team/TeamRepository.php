<?php
namespace Money\Team;
use Money\Team\TeamRepositoryInterface;
use Money\Models\Team;
use Money\Models\MatchType;
use Money\Models\Group;

class TeamRepository implements TeamRepositoryInterface 
{
	protected $team;

	public function __construct(Team $team)
	{
		$this->team = $team;
	}

	public function teams($group_letter, $match_type)
	{
		$teams = $this->team
			->when($match_type, function ($query, $match_type) {
				if($match_type == 'all')
					$query->with('teamOne', 'teamTwo');
				else {
					$type = MatchType::where('type', $match_type)->firstOrFail();

					$query->with(['teamOne' => function ($query) use($type) {
					    $query->where('match_type_id', $type->id);
					}])->with(['teamTwo' => function ($query) use($type) {
					    $query->where('match_type_id', $type->id);
					}]);
				}
	            return $query;
	        })
			->when($group_letter, function($query, $group_letter) {
				return $query = $this->byGroup($group_letter, $query);
			})
	        ->get();

		return $teams;
	}

	public function store($name, $group_letter)
	{
		try {
			$group = $this->group($group_letter);

			$group->addTeam($this->team::make($name));

			return array('Success', $group);
		}
		catch(Exception $e) {
			return $e;
		}
	}

	public function byGroup($group_letter, $query)
	{
		$group = $this->group($group_letter);
		return $query->where('group_id', $group->id);
	   
	}

	public function group($group_letter) 
	{
		return Group::where('group_letter', $group_letter)->firstOrFail();
	}
}