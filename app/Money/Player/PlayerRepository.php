<?php

namespace Money\Player;
use Money\Models\Team;
use Money\Models\Player;

/**
 * summary
 */
class PlayerRepository implements PlayerRepositoryInterface
{
    protected $playerModel;
    protected $teamModel;

	public function __construct(Player $playerModel, Team $teamModel)
	{
		$this->playerModel = $playerModel;
		$this->teamModel = $teamModel;
	}

    /**
     * Saves a new player
     * @param Request:
     *  required: team(string), player(string), shirt_number: 'int' 
     *  optional: goals(int), yellow_cards: 'int', red_cards: 'int', 
    */
	public function store($playerAttributes) 
	{
		try 
		{
			$team = $this->teamModel::where('name', $playerAttributes['team'])->firstOrFail();
			$player = $this->playerModel::firstOrNew(
			    ['shirt_number' => $playerAttributes['shirt_number'], 'team_id' => $team->id], 
			    ['name' => $playerAttributes['player'], 
			    'goals' => $playerAttributes['goals'], 
			    'yellow_cards' => $playerAttributes['yellow_cards'], 
			    'red_cards' => $playerAttributes['red_cards']
			]);
			if($player->exists) 
			{
				return array('Player already exists', $player);
			}
			else 
			{
				$player->save();
				return array('Success', $player);
			}
		}
		catch(ModelNotFoundException $e) 
		{
			return 'Team cannot be found';
		}	      
	}

	/**
     * Update a players information  
     * @param Request:
     *  required: team(string), shirt_number(string)
     *  optional: goals: 'int', yellow_cards: 'int', red_cards: 'int'  
    */
	public function update($playerAttributes) 
	{
		try 
		{
			$team = $this->teamModel::where('name', $playerAttributes['team'])->first();
			$player = $this->playerModel::where([
				['team_id', $team->id],
				['shirt_number', $playerAttributes['shirt_number']]
			])->firstOrFail();

			$player->update(['goals' => $playerAttributes['goals'], 'yellow_cards' => $playerAttributes['yellow_cards'], 'red_cards' => $playerAttributes['red_cards']]);
			return array('Success', $player);
		}
		catch(ModelNotFoundException $e) 
		{
			return 'Player cannot be found';
		}	
	}

	/**
     * Returns all players matching search queries
     * @param Request:
     *  optional: team(string), goals(int), yellow_cards: (int), red_cards: (int) 
    */
	public function players($playerAttributes) 
	{
		try 
		{
			$query = $this->playerModel::query()->with('team');
			$query->when($playerAttributes['team'], function ($q, $teamName) 
			{
				$team = $this->team::where('name', $teamName)->first();
			    return $q->where('team_id', $team->id);
			});
			$query->when($playerAttributes['goals'], function ($q, $goals) 
			{
			    return $q->where('goals', '>=', $goals)->orderBy('goals', 'desc');
			});
			$query->when($playerAttributes['yellow_cards'], function ($q, $yellow_cards) 
			{
			    return $q->where('yellow_cards', '>=', $yellow_cards)->orderBy('goals', 'desc');
			});
			$query->when($playerAttributes['red_cards'], function ($q, $red_cards) 
			{
			    return $q->where('red_cards', '>=', $red_cards)->orderBy('goals', 'desc');
			});
			
			return $query->get();
		}
		catch(Exception $e) 
		{
			return 'Cannot find team';
		}
	}
}