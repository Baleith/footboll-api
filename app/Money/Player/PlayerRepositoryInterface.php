<?php
namespace Money\Player;

interface PlayerRepositoryInterface
{
	public function store($playerAttributes);
	public function update($playerAttributes);
    public function players($playerAttribute);
}