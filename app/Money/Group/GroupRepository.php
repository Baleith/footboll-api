<?php

namespace Money\Group;
use Money\Group\GroupRepositoryInterface;
use Money\Models\Group;



class GroupRepository implements GroupRepositoryInterface
{
	private $group;

	public function __construct(Group $group)
	{
		$this->group = $group;
	}
	
    /**
     * Retrieves the top x teams with the most points in a group
     * @param top_index: int
    */
	public function getGroupsByLetter($group_letter) {
		return $this->group::with('teams')->when($group_letter, function ($query, $group) {
					return $query->where('group_letter', $group);
				})->get();
	}
}