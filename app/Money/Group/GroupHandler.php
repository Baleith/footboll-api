<?php

namespace Money\Group;

use Money\Group\GroupRepositoryInterface as Group;
use Money\Match\MatchRepositoryInterface as Match;

class GroupHandler
{
    protected $groupRepo;
    protected $matchRepo;

    public function __construct(Group $groupRepo, Match $matchRepo) {
        $this->groupRepo = $groupRepo;
        $this->matchRepo = $matchRepo;
    }

    /**
     * Retrieves all teams or all teams in a group.
     * @param Request:
	 *	optional: group_letter: 'string'
    */
	public function groupStandings($group_letter = null) {
		$groupsSortedByPoints = array();
		
		$groups = $this->groupRepo->getGroupsByLetter($group_letter);

    	foreach ($groups as $group) {
    		$groupSorted = array();

    		foreach ($group->teams as $team) {
    			$teamObject = $this->newTeamObject($team->name);
    			$matches = $this->matchRepo->getMatches($team->id);
    			
    			foreach ($matches as $match) {
    				$teamObject['points'] += $match->matchInfo->first()->points;
    			}
    			array_push($groupSorted, $teamObject);
    		}

    		$groupSorted = $this->sort($groupSorted);

			$groupObject = $this->newGroupObject($group->group_letter, $groupSorted);

			array_push($groupsSortedByPoints,  $groupObject);
    	}
    	
    	return $groupsSortedByPoints;
	}

	public function sort($groupArray) {
		usort($groupArray, function ($item1, $item2) {
			    return $item2['points'] <=> $item1['points'];
			});
		return $groupArray;
	}

	public function newGroupObject($group, $teams) {
        return array(
            "group" => $group,
			"teams" => $teams
        );
    }

    public function newTeamObject($name) {
        return array(
            "name" => $name,
			"points" => 0,
        );
    }

    public function groupByTop($top_index) {
        try {
            $groupsByTop2 = array();
            $allGroups = $this->groupStandings();
            foreach ($allGroups as $groupIndex => $group) {
                array_push($groupsByTop2, array_slice($allGroups[$groupIndex]['teams'], 0, $top_index, true));
            }
            return $groupsByTop2;
        }
        catch(Exception $e) {
            return $e;
        }
    }
}