<?php 	
namespace Money\Group;

interface GroupRepositoryInterface
{
    public function getGroupsByLetter($group_letter);
}