<?php
namespace App\Services;

use Exception;
use App\Models\Player;
use App\Models\Team;
use App\Models\Match;
use App\Http\Requests\GetPlayers;
use App\Http\Requests\StorePlayer;
use App\Http\Requests\UpdatePlayer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PlayerService {

	/**
     * Saves a new player
     * @param Request:
     *  required: team(string), player(string), shirt_number: 'int' 
     *  optional: goals(int), yellow_cards: 'int', red_cards: 'int', 
    */
	public function store(StorePlayer $request) {
		try {
			$team = Team::where('name', $request->team)->firstOrFail();
			$player = Player::firstOrNew(
			    ['shirt_number' => $request->shirt_number, 'team_id' => $team->id], 
			    ['name' => $request->player, 'goals' => $request->goals, 'yellow_cards' => $request->yellow_cards, 'red_cards' => $request->red_cards]
			);
			if($player->exists) {
				return array('Player already exists', $player);
			}
			else {
				$player->save();
				return array('Success', $player);
			}
		}
		catch(ModelNotFoundException $e) {
			return 'Team cannot be found';
		}	      
	}

	/**
     * Update a players information  
     * @param Request:
     *  required: team(string), shirt_number(string)
     *  optional: goals: 'int', yellow_cards: 'int', red_cards: 'int'  
    */
	public function update(UpdatePlayer $request) {
		try {
			$team = Team::where('name', $request->team)->first();
			$player = Player::where([
				['team_id', $team->id],
				['shirt_number', $request->shirt_number]
			])->firstOrFail();

			$player->update(['goals' => $request->goals, 'yellow_cards' => $request->yellow_cards, 'red_cards' => $request->red_cards]);
			return array('Success', $player);
		}
		catch(ModelNotFoundException $e) {
			return 'Player cannot be found';
		}	
	}

	/**
     * Returns all players matching search queries
     * @param Request:
     *  optional: team(string), goals(int), yellow_cards: (int), red_cards: (int) 
    */
	public function players(GetPlayers $request) {
		try {
			$query = Player::query()->with('team');
			$query->when(request('team'), function ($q, $teamName) {
				$team = Team::where('name', $teamName)->first();
			    return $q->where('team_id', $team->id);
			});
			$query->when(request('goals'), function ($q, $goals) {
			    return $q->where('goals', '>=', $goals)->orderBy('goals', 'desc');
			});
			$query->when(request('yellow_cards'), function ($q, $yellow_cards) {
			    return $q->where('yellow_cards', '>=', $yellow_cards)->orderBy('goals', 'desc');
			});
			$query->when(request('red_cards'), function ($q, $red_cards) {
			    return $q->where('red_cards', '>=', $red_cards)->orderBy('goals', 'desc');
			});
			
			return $query->get();
		}
		catch(Exception $e) {
			return 'Cannot find team';
		}
	}
}