<?php
namespace App\Services;

use Exception;
use App\Http\Requests\StoreTeam;
use App\Http\Requests\GetTeams;
use App\Models\Team;
use App\Models\Group;
use App\Models\MatchType;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TeamService {
	protected $repo;

	public function __construct(TeamRepository $repo)
	{
		$this->repo = $repo;
	}

	/**
     * Retrieves all teams by search queries.
     * @param Request: group_letter: 'string', match_type: 'string'
     * @return json of all teams
    */
	public function teams(GetTeams $request) {
		try {
			$query = Team::query();

			if($request->match_type) {
				$query = $this->getMatches($query, $request->match_type);
			}
			if($request->group_letter) {
				$query = $this->getMatchesByGroup($query, $request->group_letter);
			}
			
			return $query->get();
		}
		catch(Exception $e) {
			return 'Darn it, something went wrong, check your parameters!';
		}		
	}

	/**
     * Stores a new team in a certain group
     * @param Request:
     *  Required: name: 'string', group_letter: 'string'
    */
	public function store(StoreTeam $request) {
		try {
			$team = new Team(['name' => $request->name]);

			$group = Group::where('group_letter', $request->group_letter)->firstOrFail();

			$group->teams()->save($team);

			return array('Success', $team);
		}
		catch(Exception $e) {
			return $e;
		}
	}

	/**
     * Sets a where by group condition to the teams-query.
     * @param query: Query builder
     * @param group_letter: string
    */
	public function getMatchesByGroup($query, $group_letter) {
		$group = Group::where('group_letter', $group_letter)->firstOrFail();
	    return $query->where('group_id', $group->id);
	}

	/**
     * Sets the Match table relation to the team-query.
     * @param query: Query builder
     * @param match_type: string
    */
	public function getMatches($query, $match_type) {
		if($match_type == 'all') {
			$query->with('teamOne', 'teamTwo');
		}
		else {
			$type = MatchType::where('type', $match_type)->firstOrFail();

			$query->with(['teamOne' => function ($query) use($type) {
			    $query->where('match_type_id', $type->id);
			}])->with(['teamTwo' => function ($query) use($type) {
			    $query->where('match_type_id', $type->id);
			}]);
		}
		return $query;
	}
}

