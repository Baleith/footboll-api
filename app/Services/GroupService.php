<?php
namespace App\Services;

use App\Http\Requests\GroupStandings;
use App\Models\Group;
use App\Models\Team;
use App\Models\Match;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GroupService {

	/**
     * Retrieves the top x teams with the most points in a group
     * @param top_index: int
    */
	public function groupByTop($top_index) {
        try {
            $groupsByTop2 = array();
            $allGroups = $this->groupStandings();
            foreach ($allGroups as $groupIndex => $group) {
                array_push($groupsByTop2, array_slice($allGroups[$groupIndex]['teams'], 0, $top_index, true));
            }
            return $groupsByTop2;
        }
        catch(Exception $e) {
            return $e;
        }
    }

	/**
     * Retrieves all teams or all teams in a group.
     * @param Request:
	 *	optional: group_letter: 'string'
    */
	public function groupStandings($group_letter = null) {
		$groupsSortedByPoints = array();
		
		$groups = $this->getGroupsByLetter($group_letter);

    	foreach ($groups as $group) {
    		$groupSorted = array();

    		foreach ($group->teams as $team) {
    			$teamObject = $this->newTeamObject($team->name);
    			$matches = $this->getMatches($team->id);
    			
    			foreach ($matches as $match) {
    				$teamObject['points'] += $match->matchInfo->first()->points;
    			}
    			array_push($groupSorted, $teamObject);
    		}

    		$groupSorted = $this->sort($groupSorted);

			$groupObject = $this->newGroupObject($group->group_letter, $groupSorted);

			array_push($groupsSortedByPoints,  $groupObject);
    	}
    	
    	return $groupsSortedByPoints;
	}

	public function sort($groupArray) {
		usort($groupArray, function ($item1, $item2) {
			    return $item2['points'] <=> $item1['points'];
			});
		return $groupArray;
	}

	public function newGroupObject($group, $teams) {
        return array(
            "group" => $group,
			"teams" => $teams
        );
    }

    public function newTeamObject($name) {
        return array(
            "name" => $name,
			"points" => 0,
        );
    }

	public function getMatches($teamId) {
		return Match::where(function ($query) use($teamId){
                $query->where('team_1_id', $teamId)
                      ->orWhere('team_2_id', $teamId);
            })
            ->with(['matchInfo' => function($query) use($teamId) {
                $query->where('team_id', $teamId)->whereIn('winner', [0, 1]);
            }])->whereHas('matchInfo', function ($query) use ($teamId) {
                $query->whereIn('winner', [0, 1])->where('team_id', $teamId);
            })->get();
	}

	public function getGroupsByLetter($group_letter) {
		return Group::with('teams')->when($group_letter, function ($query, $group) {
					return $query->where('group_letter', $group);
				})->get();
	}
}


