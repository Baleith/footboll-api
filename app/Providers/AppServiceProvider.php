<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Money\Team\TeamRepositoryInterface', 'Money\Team\TeamRepository');
        $this->app->bind('Money\Group\GroupRepositoryInterface', 'Money\Group\GroupRepository');
        $this->app->bind('Money\Match\MatchRepositoryInterface', 'Money\Match\MatchRepository');
        $this->app->bind('Money\Player\PlayerRepositoryInterface', 'Money\Player\PlayerRepository');
    }
}
