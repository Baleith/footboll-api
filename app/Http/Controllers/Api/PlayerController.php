<?php

namespace App\Http\Controllers\Api;

use Money\Requests\GetPlayers;
use Money\Requests\StorePlayer;
use Money\Requests\UpdatePlayer;
use App\Http\Controllers\Controller;
use Money\Player\PlayerRepositoryInterface as Player;
class PlayerController extends Controller 
{

    protected $player;

    public function __construct(Player $player) 
    {
        $this->player = $player;
    }
    
    /**
     * Returns all players matching search queries
     * @param Request:
     *  optional: team(string), goals(int), yellow_cards: (int), red_cards: (int) 
    */
    public function players(GetPlayers $request) 
    {
        $playerAttributes = [
            'team' => $request->team,
            'goals' => $request->goals,
            'yellow_cards' => $request->yellow_cards,
            'red_cards' => $request->red_cards,
        ];
        return response()->json($this->player->players($playerAttributes));
    }

    /**
     * Saves a new player
     * @param Request:
     *  required: team(string), player(string), shirt_number: 'int' 
     *  optional: goals(int), yellow_cards: 'int', red_cards: 'int', 
    */
    public function store(StorePlayer $request) 
    {
        return response()->json($this->player->store($request));
    }

    /**
     * Update a players information  
     * @param Request:
     *  required: team(string), shirt_number(string)
     *  optional: goals: 'int', yellow_cards: 'int', red_cards: 'int'  
    */
    public function update(UpdatePlayer $request) 
    {
        return response()->json($this->player->update($request));
    }
}
