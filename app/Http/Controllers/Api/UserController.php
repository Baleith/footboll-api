<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use Money\Models\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Mail;
use App\Mail\UserRegistered;

class UserController extends Controller {

	public $successStatus = 200;
	
    public function login() { 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyMoney')-> accessToken; 
            return response()->json(['Success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    public function register(Request $request) { 
        $validator = Validator::make($request->all(), [ 
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'nickname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'zip' => 'required|digits_between:5,16',
            'arm' => 'required',
            'team' => 'required|string|max:55',
            'password' => 'required|min:6',
            'c_password' => 'required|same:password',
        ]);
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

		$input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['token'] =  $user->createToken('MyMoney')-> accessToken; 
        $success['name'] =  $user->name;

        Mail::to($user->email)->send(new UserRegistered($user));

		return response()->json(['Success'=>$success], $this-> successStatus); 
    }
}
