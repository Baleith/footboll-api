<?php

namespace App\Http\Controllers\Api;

use Money\Requests\StoreTeam;
use Money\Requests\GetTeams;
use App\Http\Controllers\Controller;
use Money\Team\TeamRepositoryInterface as Team;

class TeamController extends Controller {

    protected $team;

    public function __construct(Team $team) {
        $this->team = $team;
    }

    /**
     * Retrieves all teams by search queries.
     * @param Request: group_letter: 'string', match_type: 'string'
     * @return json of all teams
    */
    public function teams(GetTeams $request) {
        $teams = $this->team->teams($request->group_letter, $request->match_type);
        return response()->json($teams);
    }

    /**
     * Stores a new team in a certain group
     * @param Request:
     *  Required: name: 'string', group_letter: 'string'
    */
    public function store(StoreTeam $request) {
        return response()->json($this->team->store($request->name, $request->group_letter));
    }
}
