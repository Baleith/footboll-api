<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Money\Requests\MatchRequest;
use Money\Requests\UpdateMatch;
use Money\Match\MatchRepositoryInterface as Match;


class MatchController extends Controller
{
	protected $match;

	public function __construct(Match $match) 
    {
        $this->match = $match;
    }

	/**
     * Store a new match.
     * @param Request:
 	 * 	required: team_1: 'string', team_2: 'string', match_type: 'string', date: 'int'
	 *	optional: team_1_goals: 'int', team_2_goals: 'int'
     */
    public function store(MatchRequest $request) 
    {
        $matchAttributes = [
            'team_1' => $request->team_1,
            'team_2' => $request->team_2,
            'match_type' => $request->match_type,
            'date' => $request->date,
            'team_1_goals' => $request->team_1_goals,
            'team_2_goals' => $request->team_2_goals,
        ];

    	return response()->json($this->match->store($matchAttributes));
    }

    /**
     * Updates a match result.
     * @param Request:
     *  required: team_1: 'string', team_2: 'string', date: 'int'
     *  optional: team_1_goals: 'int', team_2_goals: 'int'
     */
    public function update(UpdateMatch $request) 
    {
        $matchAttributes = [
            'team_1' => $request->team_1,
            'team_2' => $request->team_2,
            'date' => $request->date,
            'team_1_goals' => $request->team_1_goals,
            'team_2_goals' => $request->team_2_goals,
        ];

    	return response()->json($this->match->updateMatch($matchAttributes));
    }

    /**
    * Retrieve a match by date and team names.
    * @param Request:
    * required: team_1: 'string', team_2: 'string', date: 'int'
    * @return A set of match(es) 
    */
    public function match(UpdateMatch $request) 
    {
        $matchAttributes = [
            'team_1' => $request->team_1,
            'team_2' => $request->team_2,
            'date' => $request->date,
        ];
        
		return response()->json($this->match->match($matchAttributes));
    }

    public function setMatchAttributes() 
    {

    }
}