<?php

namespace App\Http\Controllers\Api;

use Money\Requests\GroupStandings;
use App\Http\Controllers\Controller;
use Money\Group\GroupHandler;

class GroupController extends Controller {

	protected $group;

    public function __construct(GroupHandler $group) {
        $this->group = $group;
    }
    
	/**
     * Retrieves all teams or all teams in a group.
     * @param Request:
     *  optional: group_letter: 'string'
    */
	public function groupStandings(GroupStandings $request) {
        $group_letter = null;
        if($request->has('group_letter')) {
            $group_letter = $request->group_letter;
        }

        return response()->json($this->group->groupStandings($group_letter));
    }

    /**
     * Retrieves the top x teams with the most points in a group
     * @param Request:
     *  required: top_index: 'int'
    */
    public function groupByTop(GroupStandings $request) {
        $top_index = 1;
        if($request->has('top_index')) {
            $top_index = $request->top_index;
        }
        return response()->json($this->groupService->groupByTop($top_index));
    }
}