<?php

namespace App\Http\Controllers;
use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }
}



// 
// class UserController {
// 	public function index() {
// 		$users = user::whereIn('id', [1,2,3])->get();
// 		$userCounter = new UserCounter($users);
// 		return $user->asJson();
// 	}
// }

 // $eightsFinal = [
 //            [
 //                'match_type_id' => 2,
 //                'team_1' => 'France',
 //                'team_2' => 'Argentina'
 //            ],
 //            [
 //                'match_type_id' => 2,
 //                'team_1' => 'Uruguay',
 //                'team_2' => 'Portugal'
 //            ],
 //            [
 //                'match_type_id' => 2,
 //                'team_1' => 'Spain', 
 //                'team_2' => 'Russia'
 //            ],
 //            [
 //                'match_type_id' => 2,
 //                'team_1' => 'Croatia',
 //                'team_2' => 'Denmark'
 //            ],
 //            [
 //                'match_type_id' => 2,
 //                'team_1' => 'Brazil',
 //                'team_2' => 'Mexico'
 //            ],
 //            [
 //                'match_type_id' => 2,
 //                'team_1' => 'Belgium',
 //                'team_2' => 'Japan'
 //            ],
 //            [
 //                'match_type_id' => 2,
 //                'team_1' => 'Sweden',
 //                'team_2' => 'Switzerland'
 //            ],
 //            [
 //                'match_type_id' => 2,
 //                'team_1' => 'Colombia',
 //                'team_2' => 'England'
 //            ],
 //        ];

// class UserController {
// 	public function index() {
// 		$users = user::whereIn('id', [1,2,3])->get();
// 		$userCounter = new UserCounter($users);

// 		$output = new UserOutputter($users);
		
// 		return $output->view();
// 	}
// }
// class UserCounter {
// 	protected $users;

//     public function __construct($users) {
//         $this->users = $users;
//     }
//     public function count() {
//     	return $this->users->count();
//     }
// }
// class UserOutputter {
// 	protected $users;

//     public function __construct(UserCounter $users) {
//         $this->users = $users;
//     }
// 	public class json() {
// 		return response()->json($this->users->count());
// 	}
// 	public class view() {
// 		return view('user_view')->with($this->users->count());
// 	}
// }

// function userName($users) {
// 	$names = array();
// 	foreach ($users as $user) {
// 		if($user->type == 'admin') {
// 			array_push($names, $this->User->adminName());
// 		}
// 		elseif ($user->type == 'grunt') {
// 			array_push($names, $this->User->gruntName());
// 		}
// 		elseif ($user->type == 'guest') {
// 			array_push($names, $this->User->guestName());
// 		}
// 	}
// }

// /**
// * @param $user = array [
// 	new Grunt(),
// 	new Admin(),
// 	new Guest()
// 	]
// */
// function userName($users) {
// 	$user = new User();
// 	$names = [];
// 	foreach ($users as $userType) {
// 		if($userType instanceof Admin) {
// 			array_push($names, $user->adminPrivileges());
// 		}
// 		elseif ($userType instanceof Grunt) {
// 			array_push($names, $user->gruntPrivileges());
// 		}
// 		elseif ($userType instanceof Guest) {
// 			array_push($names, $user->guestPrivileges());
// 		}
// 	}
// }

// class User {
// 	public function adminPrivileges() {
// 		//return Privileges
// 	}

// 	public function gruntPrivileges() {
// 		//return Privileges
// 	}

// 	public function guestPrivileges() {
// 		//return Privileges
// 	}
// }


// /**
// * @param $user = array [
// 	new Grunt(),
// 	new Admin(),
// 	new Guest()
// 	]
// */
// function userName($users) {
// 	$names = [];
// 	foreach ($users as $user) {
// 		array_push($names, $user->privileges());
// 	}
// }

// class Admin {
// 	public function privileges() {
// 		//return Privileges
// 	}
// }
// class Grunt {
// 	public function privileges() {
// 		//return Privileges
// 	}
// }
// class Guest {
// 	public function privileges() {
// 		//return Privileges
// 	}
// }

// class User { }

// class Admin extends User {
// 	public function visitToilet($id) { }
// }
// class Guest extends User {
// 	public function visitToilet() { }
// }


// interface UserInterface {
// 	public function visitToilet();
// }
// class Admin implements UserInterface {
// 	public function visitToilet() { }
// }
// class Guest implements UserInterface {
// 	public function visitToilet() { }
// }




// interface UserInterface {
// 	public function visitToilet();

//     public function calculateWorktime($id);
// }

// class Admin implements UserInterface {
// 	public function visitToilet();

// 	public function calculateWorktime($id) { }
// }
// class Guest implements UserInterface {
// 	public function visitToilet() { }
// }



// interface UserInterface {
// 	public function visitToilet();
// }

// interface AdminUserInterface {
//     public function calculateWorktime($id);
// }

// class Admin implements UserInterface, AdminUserInterface {
// 	public function visitToilet();
	
// 	public function calculateWorktime($id) { }
// }
// class Guest implements UserInterface {
// 	public function visitToilet() { }
// }

// class Admin {
// 	protected $landRover;
// 	public function __construct(LandRover $landRover) { 
//         $this->landRover = $landRover;
//     }
//     public function drive() {
//     	$this->landRover->drive();
//     }
// }

// class LandRover() {
// 	public function drive() {
// 		return 'Driving Land Rover';
// 	}
// }

// interface CompanyVehicle {
// 	public function drive();
// }

// class CompanyCar() implements CompanyVehicleInterface {
// 	public function drive() {
// 		return 'Driving Land Rover';
// 	}
// }

// class Admin {
// 	protected $vehicle;
// 	public function __construct(CompanyVehicleInterface $vehicle) { 
//         $this->vehicle = $vehicle;
//     }
//     public function drive() {
//     	$this->vehicle->drive();
//     }
// }