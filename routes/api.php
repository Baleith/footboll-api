<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

// Route::middleware('auth:api')->group( function () {
	/* PLAYERS */
	Route::get('/players', 'API\PlayerController@players');
	Route::put('/update/player', 'API\PlayerController@update');
	Route::post('/store/player', 'API\PlayerController@store');
	/* PLAYERS */

	/* TEAMS */
	Route::get('/teams', 'API\TeamController@teams');
	Route::post('/store/team', 'API\TeamController@store');
	/* TEAMS */

	/* GROUP */
	Route::get('/group-standings', 'Api\GroupController@groupStandings');
	Route::get('/top-group', 'Api\GroupController@groupByTop');
	/* GROUP */

	/* MATCHES */
	Route::post('/match-result', 'Api\MatchController@store');
	Route::put('/update/match', 'API\MatchController@update');
	Route::get('/match', 'API\MatchController@match');
	/* MATCHES */
// });