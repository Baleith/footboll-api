<?php

use Illuminate\Database\Seeder;
use Money\Models\Team;
use Money\Models\Player;

class PlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file_n = storage_path('app/public/wcinfo.csv');
        
        $file = fopen($file_n, "r");
        $header = true;
        $i = 0;

        while ($csvLine = fgetcsv($file, 1000, ",")) {
            if ($header) {
                $header = false;
            } 
            else {
                $player = new Player(['name' => $csvLine[3], 'shirt_number' => $i]);

                $team = Team::where('name', $csvLine[0])->firstOrFail();

                $team->players()->save($player);
            }
            $i++;
        }
    }
}
