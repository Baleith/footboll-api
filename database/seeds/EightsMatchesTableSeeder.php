<?php

use Illuminate\Database\Seeder;
use Money\Group\GroupHandler;
use Money\Models\Team;
use Money\Models\Match;
use Money\Models\MatchInfo;

class EightsMatchesTableSeeder extends Seeder
{
    protected $group;
    public function __construct(GroupHandler $group)
    {
        $this->group = $group;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = 20180701;
        $groupByTop = $this->group->groupByTop(2);
        foreach ($groupByTop as $key => $teams) {
            $teamName = Team::whereIn('name', [$teams[0]['name'], $teams[1]['name']])->get();
            $match = Match::create([
                    'match_type_id' => 2,
                    'team_1_id' => $teamName[0]->id,
                    'team_2_id' => $teamName[1]->id,
                    'team_1' => $teamName[0]->name,
                    'team_2' => $teamName[1]->name,
                ]);
            $matchInfos = array(
                array('match_id' => $match->id, 'team_id'=> $teamName[0]->id, 'goals'=> null, 'points'=> null, 'winner'=> null, 'draw' => null, 'date' => $date),
                array('match_id' => $match->id, 'team_id'=> $teamName[1]->id, 'goals'=> null, 'points'=> null, 'winner'=> null, 'draw' => null, 'date' => $date),
            );
            foreach ($matchInfos as $key => $matchInfo) {
                $record = MatchInfo::create($matchInfo);
            }
            $date++;
        }
    }
}
