<?php

use Illuminate\Database\Seeder;
use Money\Models\Group;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach (range('A', 'H') as $group_letter) {
		    Group::firstOrCreate([
                'group_letter' => $group_letter
            ]);
		}
    }
}
