<?php

use Illuminate\Database\Seeder;
use Money\Models\Team;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$json = file_get_contents('https://worldcup.sfg.io/teams');
        $teams = json_decode($json);

        foreach ($teams as $team) {
            Team::firstOrCreate(
                ['name' => $team->country],
                ['group_id' => $team->group_id]
            );
        }
    }
}
