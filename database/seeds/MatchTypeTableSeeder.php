<?php

use Illuminate\Database\Seeder;

class MatchTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
        	['type' => 'Group'], 
        	['type' => 'Eight'], 
        	['type' => 'Quarter'], 
        	['type' => 'Semi'], 
        	['type' => 'Final']
        ];

        DB::table('match_types')->insert($types);
    }
}
