<?php

use Illuminate\Database\Seeder;
use Money\Models\Team;
use Money\Models\Match;
use Money\Models\MatchType;
use Money\Models\MatchInfo;

class MatchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = 20180101;
        $teams = Team::all();
        $min = count($teams) / 2;
        for ($i=0; $i < $min; $i++) { 
            for ($j=0; $j < 3;  $j++) { 
               
                \DB::transaction(function() use ($teams, $i, $min, $date) {
                    $group = MatchType::where('type', 'group')->firstOrFail();

                    $match = Match::create([
                        'match_type_id' => $group->id,
                        'team_1_id' => $teams[$i]->id,
                        'team_2_id' => $teams[$i+$min]->id,
                        'team_1' => $teams[$i]->name, 
                        'team_2' => $teams[$i+$min]->name,
                    ]);

                    $matchInfos = array(
                        array('match_id' => $match->id, 'team_id'=> $teams[$i]->id, 'goals'=> mt_rand(0,10), 'points'=> mt_rand(0,3), 'winner'=> 1, 'draw' => 0, 'date' => $date),
                        array('match_id' => $match->id, 'team_id'=> $teams[$i+$min]->id, 'goals'=> mt_rand(0,10), 'points'=> mt_rand(0,3), 'winner'=> 0, 'draw' => 0, 'date' => $date),
                    );
                    foreach ($matchInfos as $key => $matchInfo) {
                        $record = MatchInfo::create($matchInfo);
                    }
                });
                $date++;
            }
        }

        // DB::table('matches')->insert($eightsFinal);
    }
}
